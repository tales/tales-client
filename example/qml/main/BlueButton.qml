import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

/**
 * A blue button suitable for use on frames.
 */
T.Button {
    id: control

    property bool keepPressed: false

    implicitHeight: Math.max(sizeLabel.height + 10, 30)
    implicitWidth: Math.max(sizeLabel.width + 20, 30)

    padding: 6
    horizontalPadding: padding + 2
    spacing: 6

    icon.width: 24
    icon.height: 24
    icon.color: control.checked || control.highlighted ? control.palette.brightText :
                control.flat && !control.down ? (control.visualFocus ? control.palette.highlight : control.palette.windowText) : control.palette.buttonText

    contentItem: Item {
        Text {
            id: label
            text: control.text
            color: "white"
            anchors.centerIn: parent
            font.pixelSize: 14
        }

        Image {
            id: icon
            source: control.icon.source
            anchors.centerIn: parent
            smooth: false
        }

        states: [
            State {
                name: "pressed"
                when: control.pressed
            },
            State {
                name: "disabled"
                when: !control.enabled
                PropertyChanges {
                    target: label
                    opacity: 0.7
                }
            }
        ]
    }

    background: BorderImage {
        source: {
            if (control.pressed || control.keepPressed)
                "images/frame_button_pressed.png"
            else
                "images/frame_button.png"
        }

        smooth: false

        border.bottom: 5
        border.top: 5
        border.right: 5
        border.left: 5
    }

    Text {
        id: sizeLabel
        visible: false
        text: control.text
        font.pixelSize: 14
    }
}
