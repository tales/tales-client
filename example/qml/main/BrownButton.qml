import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

/**
 * A brown button suitable for use on scrolls.
 */
T.Button {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(sizeLabel.height + 2 * 5, 20)

    property bool keepPressed: false

    padding: 6
    horizontalPadding: padding + 2
    spacing: 6

    icon.width: 24
    icon.height: 24
    icon.color: control.checked || control.highlighted ? control.palette.brightText :
                control.flat && !control.down ? (control.visualFocus ? control.palette.highlight : control.palette.windowText) : control.palette.buttonText

    contentItem: Item {
        Text {
            id: label
            text: control.text
            color: "#3f2b25"
            font.pixelSize: 14
            wrapMode: Text.WordWrap

            anchors.right: parent.right
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 7
            anchors.leftMargin: 7
        }

        Image {
            id: icon
            source: control.icon.source
            anchors.centerIn: parent
            smooth: false
        }

        states: [
            State {
                name: "pressed"
                when: control.pressed || control.keepPressed
                PropertyChanges {
                    target: label
                    anchors.verticalCenterOffset: 1
                }
                PropertyChanges {
                    target: icon
                    anchors.verticalCenterOffset: 1
                }
            },
            State {
                name: "disabled"
                when: !control.enabled
                PropertyChanges {
                    target: label
                    opacity: 0.7
                }
            }
        ]
    }

    background: BorderImage {
        source: {
            if (control.pressed || control.keepPressed)
                "images/scroll_button_pressed.png"
            else if (control.focus)
                "images/scroll_button_focused.png"
            else
                "images/scroll_button.png"
        }

        smooth: false

        border.bottom: 5
        border.top: 5
        border.right: 5
        border.left: 5
    }

    Text {
        id: sizeLabel
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 7
        anchors.leftMargin: 7
        visible: false
        text: control.text
        font.pixelSize: 14
        wrapMode: Text.WordWrap
    }
}
