#!/bin/bash -xe

./ci-build.sh

wget --no-verbose "https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage"
wget --no-verbose "https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage"
chmod +x linuxdeploy*.AppImage

export QML_SOURCES_PATHS=$PWD/example/qml

mkdir -p install/usr/share/icons/hicolor/scalable/apps
mkdir -p install/usr/share/applications
cp example/tales-icon.svg install/usr/share/icons/hicolor/scalable/apps/tales.svg
cp org.sourceoftales.Client.desktop install/usr/share/applications

./linuxdeploy-x86_64.AppImage --appdir install --plugin qt
rm -rfv install/usr/plugins/bearer
./linuxdeploy-x86_64.AppImage --appdir install --output appimage
