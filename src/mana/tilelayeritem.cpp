/*
 * Mana Mobile
 * Copyright (C) 2010  Thorbjørn Lindeijer 
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "tilelayeritem.h"

#include "tiled/tile.h"
#include "tiled/tilelayer.h"
#include "tiled/map.h"
#include "tiled/maprenderer.h"

#include "mana/mapitem.h"
#include "mana/resource/imageresource.h"
#include "mana/resource/mapresource.h"
#include "mana/tilesnode.h"

using namespace Tiled;
using namespace Mana;

namespace {

/**
 * Returns the texture of a given tileset, or 0 if the image has not been
 * loaded yet.
 */
inline QSGTexture *tilesetTexture(Tileset *tileset,
                                  const MapItem *mapItem,
                                  QQuickWindow *window)
{
    if (const ImageResource *image = mapItem->mapResource()->tilesetImage(tileset))
        if (image->isReady())
            return image->texture(window);
    return nullptr;
}

/**
 * This helper class exists mainly to avoid redoing calculations that only need
 * to be done once per tileset.
 */
struct TilesetHelper
{
    TilesetHelper(const MapItem *mapItem)
        : mMapItem(mapItem)
        , mWindow(mapItem->window())
    {
    }

    Tileset *tileset() const { return mTileset; }
    QSGTexture *texture() const { return mTexture; }

    void setTileset(Tileset *tileset)
    {
        mTileset = tileset;
        mTexture = tilesetTexture(tileset, mMapItem, mWindow);
        if (!mTexture)
            return;

        const int tileSpacing = tileset->tileSpacing();
        mMargin = tileset->margin();
        mTileHSpace = tileset->tileWidth() + tileSpacing;
        mTileVSpace = tileset->tileHeight() + tileSpacing;

        const QSize tilesetSize = mTexture->textureSize();
        const int availableWidth = tilesetSize.width() + tileSpacing - mMargin;
        mTilesPerRow = qMax(availableWidth / mTileHSpace, 1);
    }

    void setTextureCoordinates(TileData &data, const Cell &cell) const
    {
        const int tileId = cell.tileId();
        const int column = tileId % mTilesPerRow;
        const int row = tileId / mTilesPerRow;

        data.tx = column * mTileHSpace + mMargin;
        data.ty = row * mTileVSpace + mMargin;
    }

private:
    const MapItem *mMapItem;
    QQuickWindow *mWindow;
    Tileset *mTileset = nullptr;
    QSGTexture *mTexture = nullptr;
    int mMargin = 0;
    int mTileHSpace = 0;
    int mTileVSpace = 0;
    int mTilesPerRow = 0;
};

} // anonymous namespace


TileLayerItem::TileLayerItem(TileLayer *layer, MapRenderer *renderer,
                             MapItem *parent)
    : QQuickItem(parent)
    , mLayer(layer)
    , mRenderer(renderer)
    , mVisibleArea(parent->visibleArea())
{
    setFlag(ItemHasContents);
    layerVisibilityChanged();

    syncWithTileLayer();
    setOpacity(mLayer->opacity());
}

void TileLayerItem::syncWithTileLayer()
{
    const QRectF boundingRect = mRenderer->boundingRect(mLayer->rect());
    setPosition(boundingRect.topLeft());
    setSize(boundingRect.size());
}



QSGNode *TileLayerItem::updatePaintNode(QSGNode *node,
                                        QQuickItem::UpdatePaintNodeData *)
{
    delete node;
    node = new QSGNode;
    node->setFlag(QSGNode::OwnedByParent);

    TilesetHelper helper(static_cast<MapItem*>(parentItem()));

    QVector<TileData> tileData;
    tileData.reserve(TilesNode::MaxTileCount);

    /**
     * Draws the tiles by adding nodes to the scene graph. When sequentially
     * drawn tiles are using the same tileset, they will share a single
     * geometry node.
     */
    auto tileRenderFunction = [&](QPoint tilePos, const QPointF &screenPos) {
        const Cell &cell = mLayer->cellAt(tilePos);
        Tileset *tileset = cell.tileset();
        if (!tileset)
            return;

        if (tileset != helper.tileset() || tileData.size() == TilesNode::MaxTileCount) {
            if (!tileData.isEmpty()) {
                node->appendChildNode(new TilesNode(helper.texture(), tileData));
                tileData.clear();
            }

            helper.setTileset(tileset);
        }

        if (!helper.texture())
            return;

        const auto offset = tileset->tileOffset();
        const QSize size = tileset->tileSize();

        TileData data;
        data.x = static_cast<float>(screenPos.x()) + offset.x();
        data.y = static_cast<float>(screenPos.y() - size.height()) + offset.y();
        data.width = static_cast<float>(size.width());
        data.height = static_cast<float>(size.height());
        data.flippedHorizontally = cell.flippedHorizontally();
        data.flippedVertically = cell.flippedVertically();
        helper.setTextureCoordinates(data, cell);
        tileData.append(data);
    };

    mRenderer->drawTileLayer(tileRenderFunction, mVisibleArea);

    if (!tileData.isEmpty())
        node->appendChildNode(new TilesNode(helper.texture(), tileData));

    return node;
}

void TileLayerItem::updateVisibleTiles()
{
    const MapItem *mapItem = static_cast<MapItem*>(parentItem());

    QRectF rect = mapItem->visibleArea();

    QMargins drawMargins = mLayer->drawMargins();
    drawMargins.setTop(drawMargins.top() - mRenderer->map()->tileHeight());
    drawMargins.setRight(drawMargins.right() - mRenderer->map()->tileWidth());

    rect.adjust(-drawMargins.right(),
                -drawMargins.bottom(),
                drawMargins.left(),
                drawMargins.top());

    rect &= mRenderer->boundingRect(mLayer->localBounds());

    if (mVisibleArea != rect) {
        mVisibleArea = rect;
        update();
    }
}

void TileLayerItem::layerVisibilityChanged()
{
    const bool visible = mLayer->isVisible();
    setVisible(visible);

    MapItem *parent = qobject_cast<MapItem*>(parentItem());
    if (visible) {
        updateVisibleTiles();

        if (parent)
            connect(parent, &MapItem::visibleAreaChanged, this, &TileLayerItem::updateVisibleTiles);
    } else {
        if (parent)
            disconnect(parent, &MapItem::visibleAreaChanged, this, &TileLayerItem::updateVisibleTiles);
    }
}


TileItem::TileItem(const Cell &cell, QPoint position, MapItem *parent)
    : QQuickItem(parent)
    , mCell(cell)
    , mPosition(position)
{
    setFlag(ItemHasContents);
    setZ(position.y() * parent->mapResource()->map()->tileHeight());
}

QSGNode *TileItem::updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *)
{
    if (!node) {
        const MapItem *mapItem = static_cast<MapItem*>(parent());

        TilesetHelper helper(mapItem);
        Tileset *tileset = mCell.tileset();
        helper.setTileset(tileset);

        if (!helper.texture())
            return nullptr;

        const Map *map = mapItem->mapResource()->map();
        const int tileWidth = map->tileWidth();
        const int tileHeight = map->tileHeight();

        const QSize size = tileset->tileSize();
        const QPoint offset = tileset->tileOffset();

        QVector<TileData> data(1);
        data[0].x = mPosition.x() * tileWidth + offset.x();
        data[0].y = (mPosition.y() + 1) * tileHeight - tileset->tileHeight() + offset.y();
        data[0].width = size.width();
        data[0].height = size.height();
        helper.setTextureCoordinates(data[0], mCell);

        node = new TilesNode(helper.texture(), data);
    }

    return node;
}
