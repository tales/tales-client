#!/bin/bash -xe

SRC_DIR=$PWD

mkdir build
pushd build

qmake $SRC_DIR
make -j$(nproc)
make install INSTALL_ROOT=$SRC_DIR/install/

popd
