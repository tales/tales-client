/*
 * Mana QML plugin
 * Copyright (C) 2011  Jared Adams
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "itemdb.h"

#include "mana/resourcemanager.h"
#include "mana/xmlreader.h"

#include "hairdb.h"
#include "racedb.h"

#include <QDebug>
#include <QNetworkReply>

using namespace Mana;

static const ItemDB::Stat fields[] = {
    ItemDB::Stat("attack",  "Attack %+d"),
    ItemDB::Stat("defense", "Defense %+d"),
    ItemDB::Stat("hp",      "HP %+d"),
    ItemDB::Stat("mp",      "MP %+d")
};

void ItemDB::setStatsList(QList<Stat> stats)
{
    mExtraStats = stats;
}

QList<ItemInfo*> ItemDB::items() const
{
    return mItems.values();
}

static ItemInfo::Type itemTypeFromString(QStringView name)
{
    if (name == QLatin1String("generic"))
        return ItemInfo::ITEM_UNUSABLE;
    if (name == QLatin1String("usable"))
        return ItemInfo::ITEM_USABLE;
    if (name == QLatin1String("equip-1hand"))
        return ItemInfo::ITEM_EQUIPMENT_ONE_HAND_WEAPON;
    if (name == QLatin1String("equip-2hand"))
        return ItemInfo::ITEM_EQUIPMENT_TWO_HANDS_WEAPON;
    if (name == QLatin1String("equip-torso"))
        return ItemInfo::ITEM_EQUIPMENT_TORSO;
    if (name == QLatin1String("equip-arms"))
        return ItemInfo::ITEM_EQUIPMENT_ARMS;
    if (name == QLatin1String("equip-head"))
        return ItemInfo::ITEM_EQUIPMENT_HEAD;
    if (name == QLatin1String("equip-legs"))
        return ItemInfo::ITEM_EQUIPMENT_LEGS;
    if (name == QLatin1String("equip-shield"))
        return ItemInfo::ITEM_EQUIPMENT_SHIELD;
    if (name == QLatin1String("equip-ring"))
        return ItemInfo::ITEM_EQUIPMENT_RING;
    if (name == QLatin1String("equip-charm"))
        return ItemInfo::ITEM_EQUIPMENT_CHARM;
    if (name == QLatin1String("equip-necklace"))
        return ItemInfo::ITEM_EQUIPMENT_NECKLACE;
    if (name == QLatin1String("equip-feet"))
        return ItemInfo::ITEM_EQUIPMENT_FEET;
    if (name == QLatin1String("equip-ammo"))
        return ItemInfo::ITEM_EQUIPMENT_AMMO;
    if (name == QLatin1String("racesprite"))
        return ItemInfo::ITEM_SPRITE_RACE;
    if (name == QLatin1String("hairsprite"))
        return ItemInfo::ITEM_SPRITE_HAIR;

    return ItemInfo::ITEM_UNUSABLE;
}

static BeingGender genderFromString(QStringView gender)
{
    if (gender == QLatin1String("male"))
        return GENDER_MALE;
    if (gender == QLatin1String("female"))
        return GENDER_FEMALE;

    return GENDER_UNSPECIFIED;
}

ItemDB *ItemDB::mInstance = nullptr;

ItemDB::ItemDB(QObject *parent)
    : QObject(parent)
{
    Q_ASSERT(!mInstance);
    mInstance = this;
}

void ItemDB::load() const
{
    if (mLoaded)
        return;

    QNetworkReply *reply = ResourceManager::instance()->requestFile(ITEMS_DB_FILE);
    connect(reply, &QNetworkReply::finished, this, &ItemDB::fileReady);
}

void ItemDB::unload()
{
    qDeleteAll(mItems);
    mItems.clear();

    mLoaded = false;
    emit itemsChanged();
}

ItemInfo *ItemDB::getInfo(int id) const
{
    return mItems.value(id);
}

ItemInfo *ItemDB::readItem(XmlReader &xml)
{
    const QXmlStreamAttributes attr = xml.attributes();
    int id = attr.value("id").toInt();

    if (!id) {
        qDebug() << "Bad or missing item id at line " << xml.lineNumber();
        xml.skipCurrentElement();
        return nullptr;
    }

    // TODO: Move races to a seperate file and move parsing to racedb
    if (attr.value("type") == QLatin1String("racesprite")) { // Race "item"
        auto *raceInfo = new RaceInfo(-id);
        raceInfo->setName(attr.value("name").toString());

        while (xml.readNextStartElement()) {
            if (xml.name() == QLatin1String("sprite")) {
                const BeingGender gender =
                        genderFromString(xml.attributes().value("gender"));
                SpriteReference *sprite =
                        SpriteReference::readSprite(xml, raceInfo);

                raceInfo->setSprite(gender, sprite);
            } else {
                xml.skipCurrentElement();
            }
        }

        RaceDB::instance()->setInfo(-id, raceInfo);
        return nullptr;
    }

    // TODO: Move hairs to a seperate file and move parsing to hairdb
    if (attr.value("type") == QLatin1String("hairsprite")) { // Hair "item"
        // invert the negative id for now
        id = -id;
        auto *hairInfo = new HairInfo(id, HairDB::instance());
        hairInfo->setName(attr.value("name").toString());

        while (xml.readNextStartElement()) {
            if (xml.name() == QLatin1String("sprite")) {
                const BeingGender gender =
                        genderFromString(xml.attributes().value("gender"));
                SpriteReference *sprite =
                        SpriteReference::readSprite(xml, hairInfo);

                hairInfo->setSprite(gender, sprite);
            } else {
                xml.skipCurrentElement();
            }
        }

        HairDB::instance()->setInfo(id, hairInfo);
        return nullptr;
    }

    auto item = new ItemInfo(id, this);

    item->setType(itemTypeFromString(attr.value("type")));
    item->setName(attr.value("name").toString());
    item->setDescription(attr.value("description").toString());
    item->setWeight(attr.value("weight").toInt());
    item->setValue(attr.value("value").toInt());

    SpriteDisplay display;
    item->setImage(attr.value("image").toString());

    if (item->name().isEmpty())
        item->setName("unnamed");

    QStringList effects;

    for (const auto &field : fields) {
        int value = attr.value(field.tag).toInt();
        if (value)
            effects.append(field.format.arg(value));
    }

//    for (Stat stat : mExtraStats) {
//        int value = xml.intAttribute(stat.tag);
//        if (value)
//            effects.append(stat.format.arg(value));
//    }

    const auto temp = attr.value("effect");
    if (!temp.isEmpty())
        effects.append(temp.toString());

    item->setEffects(effects);

    while (xml.readNextStartElement()) {
        if (xml.name() == QLatin1String("sprite")) {
            const BeingGender gender =
                    genderFromString(xml.attributes().value("gender"));
            SpriteReference *sprite = SpriteReference::readSprite(xml, item);

            item->addSprite(gender, sprite);
        } else if (xml.name() == QLatin1String("particlefx")) {
            item->setParticleFx(xml.readElementText());
        } else if (xml.name() == QLatin1String("sound")) {
            xml.skipCurrentElement(); // TODO
        } else if (xml.name() == QLatin1String("floor")) {
            while (xml.readNextStartElement()) {
                if (xml.name() == QLatin1String("sprite")) {
                    display.sprites.append(
                                SpriteReference::readSprite(xml, item));
                } else if (xml.name() == QLatin1String("particlefx")) {
                    display.particles.append(xml.readElementText());
                } else {
                    xml.readUnknownElement();
                }
            }
        } else {
            xml.skipCurrentElement();
        }
    }

    item->setDisplay(display);

    return item;
}

void ItemDB::fileReady()
{
    auto *reply = static_cast<QNetworkReply *>(sender());
    reply->deleteLater();
    XmlReader xml(reply);

    if (!xml.readNextStartElement() || xml.name() != QLatin1String("items")) {
        qWarning() << "Error loading items.xml";
        return;
    }

    while (xml.readNextStartElement()) {
        if (xml.name() == QLatin1String("item")) {
            if (ItemInfo *item = readItem(xml))
                mItems[item->id()] = item;
        } else if (xml.name() == QLatin1String("version")) {
            xml.skipCurrentElement(); // Ignore for now
        } else {
            xml.readUnknownElement();
        }
    }

    mLoaded = true;
    RaceDB::instance()->mLoaded = true;
    HairDB::instance()->mLoaded = true;
    emit itemsChanged();
    emit loaded();
    emit RaceDB::instance()->racesChanged();
    emit RaceDB::instance()->loaded();
    emit HairDB::instance()->hairsChanged();
    emit HairDB::instance()->loaded();
}
