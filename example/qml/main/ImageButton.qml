import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

T.Button {
    id: control

    property string baseName: "images/roundbutton"
    property alias imagePath: image.source

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

    padding: 6
    horizontalPadding: padding + 2
    spacing: 6

    icon.width: 24
    icon.height: 24
    icon.color: control.checked || control.highlighted ? control.palette.brightText :
                control.flat && !control.down ? (control.visualFocus ? control.palette.highlight : control.palette.windowText) : control.palette.buttonText

    contentItem: Image {
        id: image
        smooth: false;
        anchors.centerIn: parent;
    }

    background: Image {
        smooth: false;
        source: {
            if (control.pressed)
                return baseName + "_pressed.png";
            return baseName + ".png";
        }
    }

    states: [
        State {
            name: "pressed"
            when: control.pressed
            PropertyChanges {
                target: control;
                anchors.horizontalCenterOffset: 1;
                anchors.verticalCenterOffset: 1;
            }
        }
    ]
}
