TEMPLATE = app

SOURCES += main.cpp
RESOURCES += main.qrc

TARGET = tales
QT += qml quick

CONFIG += c++17

# Depend on "mana" library in "../src"
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../src/release/ -lmana
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../src/debug/ -lmana
else:unix: LIBS += -L$$OUT_PWD/../src/$$QT_ARCH/ -lmana
INCLUDEPATH += $$PWD/../src
DEPENDPATH += $$PWD/../src
win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../src/release/libmana.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../src/debug/libmana.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../src/release/mana.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../src/debug/mana.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../src/$$QT_ARCH/libmana.a

# Needed by libtiled
LIBS += -lz

# Needed by enet
win32:LIBS += -lws2_32 -lwinmm

OTHER_FILES += \
    android/res/drawable/splash.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
ANDROID_VERSION_CODE = 1
ANDROID_TARGET_SDK_VERSION = 34

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/res/values/strings.xml

linux {
    target.path = /usr/bin
    INSTALLS += target
}

android: include($$PWD/../external/openssl/openssl.pri)
