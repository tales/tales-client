/*
 * Mana QML plugin
 * Copyright (C) 2010  Thorbjørn Lindeijer
 * Copyright (C) 2013  Erik Schilling <ablu.erikschilling@googlemail.com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "manaplugin.h"

#include "abilitylistmodel.h"
#include "accountclient.h"
#include "attributelistmodel.h"
#include "beinglistmodel.h"
#include "characterlistmodel.h"
#include "chatclient.h"
#include "droplistmodel.h"
#include "enetclient.h"
#include "gameclient.h"
#include "inventorylistmodel.h"
#include "mapitem.h"
#include "resourcelistmodel.h"
#include "resourcemanager.h"
#include "settings.h"
#include "shoplistmodel.h"
#include "spriteitem.h"
#include "spritelistmodel.h"
#include "questloglistmodel.h"

#include "resource/abilitydb.h"
#include "resource/attributedb.h"
#include "resource/hairdb.h"
#include "resource/itemdb.h"
#include "resource/mapresource.h"
#include "resource/monsterdb.h"
#include "resource/npcdb.h"
#include "resource/racedb.h"

#include <QQmlEngine>
#include <QQmlContext>

#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
template <typename T>
static void qmlRegisterAnonymousType(const char *uri, int versionMajor)
{
    qmlRegisterType<T>();
}
#endif

ManaPlugin::ManaPlugin(QObject *parent) :
    QQmlExtensionPlugin(parent)
{
}

void ManaPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    Q_UNUSED(uri)

    Q_INIT_RESOURCE(mana);
    engine->addImportPath("qrc:/mana/qml/");

    auto *resourceManager = new Mana::ResourceManager(engine);
    auto *abilityDB = new Mana::AbilityDB(engine);
    auto *attributeDB = new Mana::AttributeDB(engine);
    auto *hairDB = new Mana::HairDB(engine);
    auto *itemDB = new Mana::ItemDB(engine);
    auto *monsterDB = new Mana::MonsterDB(engine);
    auto *npcDB = new Mana::NpcDB(engine);
    auto *raceDB = new Mana::RaceDB(engine);

    QQmlContext *context = engine->rootContext();
    context->setContextProperty("resourceManager", resourceManager);
    context->setContextProperty("abilityDB", abilityDB);
    context->setContextProperty("attributeDB", attributeDB);
    context->setContextProperty("hairDB", hairDB);
    context->setContextProperty("itemDB", itemDB);
    context->setContextProperty("monsterDB", monsterDB);
    context->setContextProperty("npcDB", npcDB);
    context->setContextProperty("raceDB", raceDB);

    int errorCode = enet_initialize();
    Q_ASSERT(errorCode == 0);
    atexit(enet_deinitialize);
}

void ManaPlugin::registerTypes(const char *uri)
{
    // @uri Mana
    Q_ASSERT(uri == QLatin1String("Mana"));

    qmlRegisterUncreatableType<Mana::ENetClient>(uri, 1, 0, "ENetClient", "Use a derived class");

    qmlRegisterType<Mana::AccountClient>(uri, 1, 0, "AccountClient");
    qmlRegisterType<Mana::ChatClient>(uri, 1, 0, "ChatClient");
    qmlRegisterType<Mana::GameClient>(uri, 1, 0, "GameClient");
    qmlRegisterType<Mana::Settings>(uri, 1, 0, "Settings");
    qmlRegisterType<Mana::SpriteItem>(uri, 1, 0, "Sprite");
    qmlRegisterUncreatableType<Mana::Action>(uri, 1, 0, "Action", "Only exposed for direction enum");

    qmlRegisterAnonymousType<Mana::CharacterListModel>(uri, 1);
    qmlRegisterAnonymousType<Mana::BeingListModel>(uri, 1);
    qmlRegisterUncreatableType<Mana::Being>(uri, 1, 0, "Being", "Managed on C++ side");
    qmlRegisterType<Mana::Character>(uri, 1, 0, "Character");

    qmlRegisterAnonymousType<Mana::SpriteListModel>(uri, 1);
    qmlRegisterType<Mana::SpriteReference>(uri, 1, 0, "SpriteReference");

    qmlRegisterAnonymousType<Mana::AbilityListModel>(uri, 1);

    qmlRegisterAnonymousType<Mana::AttributeListModel>(uri, 1);
    qmlRegisterAnonymousType<Mana::AttributeInfo>(uri, 1);
    qmlRegisterAnonymousType<Mana::AttributeValue>(uri, 1);

    qmlRegisterAnonymousType<Mana::Drop>(uri, 1);
    qmlRegisterAnonymousType<Mana::DropListModel>(uri, 1);

    qmlRegisterAnonymousType<Mana::InventoryListModel>(uri, 1);
    qmlRegisterAnonymousType<Mana::ShopListModel>(uri, 1);

    qmlRegisterAnonymousType<Mana::ResourceManager>(uri, 1);
    qmlRegisterUncreatableType<Mana::Resource>(uri, 1, 0, "Resource", "Base of all resources");
    qmlRegisterAnonymousType<Mana::MapResource>(uri, 1);
    qmlRegisterAnonymousType<Mana::ResourceListModel>(uri, 1);

    qmlRegisterUncreatableType<Mana::Quest>(uri, 1, 0, "Quest", "Managed on C++ side");
    qmlRegisterAnonymousType<Mana::QuestlogListModel>(uri, 1);

    qmlRegisterAnonymousType<Mana::Ability>(uri, 1);
    qmlRegisterAnonymousType<Mana::AbilityDB>(uri, 1);
    qmlRegisterUncreatableType<Mana::AbilityInfo>(uri, 1, 0, "AbilityInfo", "Managed on C++ side");

    qmlRegisterAnonymousType<Mana::AttributeDB>(uri, 1);

    qmlRegisterAnonymousType<Mana::ItemDB>(uri, 1);
    qmlRegisterAnonymousType<Mana::ItemInfo>(uri, 1);
    qmlRegisterAnonymousType<Mana::ItemInstance>(uri, 1);

    qmlRegisterAnonymousType<Mana::MonsterDB>(uri, 1);
    qmlRegisterAnonymousType<Mana::NpcDB>(uri, 1);
    qmlRegisterAnonymousType<Mana::RaceDB>(uri, 1);
    qmlRegisterAnonymousType<Mana::HairInfo>(uri, 1);

    qmlRegisterType<Mana::MapItem>(uri, 1, 0, "TileMap");
}
