#!/bin/bash -xe

./ci-build.sh

SIGNING_ARGS=""
if [[ "$KEYPASS" ]]; then
    SIGNING_ARGS="--sign $KEYSTORE $KEYALIAS --keypass $KEYPASS --storepass $KEYPASS"
fi

androiddeployqt --input build/example/*.json --output $PWD/install/ $SIGNING_ARGS
