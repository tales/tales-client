import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Templates 2.15 as T

T.Button {
    id: control

    implicitWidth: Math.max(sizeLabel.width + 20 + 20, 200);
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

    padding: 6
    horizontalPadding: padding + 2
    spacing: 6

    icon.width: 24
    icon.height: 24
    icon.color: control.checked || control.highlighted ? control.palette.brightText :
                control.flat && !control.down ? (control.visualFocus ? control.palette.highlight : control.palette.windowText) : control.palette.buttonText

    contentItem: Item {
        TextShadow {
            target: label;
            color: "white";
            opacity: 0.7;
        }
        Text {
            id: label
            text: control.text
            anchors.centerIn: parent
            font.pixelSize: (control.height - 10) * 0.7;
            opacity: 0.8;
        }

        states: [
            State {
                name: "pressed"
                when: control.pressed
                PropertyChanges {
                    target: label
                    anchors.horizontalCenterOffset: 1
                    anchors.verticalCenterOffset: 1
                }
            },
            State {
                name: "disabled"
                when: !control.enabled
                PropertyChanges {
                    target: label;
                    opacity: 0.7;
                }
            }
        ]
    }

    background: BorderImage {
        source: {
            const baseName = "images/bigbutton";
            if (control.pressed)
                baseName + "_pressed.png";
            else if (!control.enabled)
                baseName + "_disabled.png";
            else if (control.hovered || control.activeFocus)
                baseName + "_hovered.png";
            else
                baseName + ".png";
        }

        border.bottom: 20;
        border.top: 26;
        border.right: 100;
        border.left: 100;
    }

    Text {
        id: sizeLabel
        visible: false
        text: control.text
        font.pixelSize: (control.height - 10) * 0.7;
    }
}
