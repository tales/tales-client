/*
 * Mana QML plugin
 * Copyright (C) 2010-2013  Thorbjørn Lindeijer
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MANA_MAPRESOURCE_H
#define MANA_MAPRESOURCE_H

#include "resource.h"

#include "tiled/tileset.h"

#include <QHash>
#include <QSet>

class QNetworkReply;

namespace Tiled {
class Map;
class TileLayer;
}

namespace Mana {

class ImageResource;

class MapResource : public Resource
{
    Q_OBJECT

public:
    explicit MapResource(const QUrl &url,
                         const QString &path,
                         QObject *parent = nullptr);
    ~MapResource() override;

    const Tiled::Map *map() const;
    const Tiled::TileLayer *collisionLayer() const;
    const ImageResource *tilesetImage(Tiled::Tileset *tileset) const;

private:
    void mapFinished();
    void tilesetFinished();
    void imageStatusChanged();

    QNetworkReply *finishReply();
    void checkReady();
    void requestTilesetImage(Tiled::Tileset *tileset);

    QString mPath;
    std::unique_ptr<Tiled::Map> mMap;
    Tiled::TileLayer *mCollisionLayer = nullptr;

    QList<QNetworkReply*> mPendingResources;
    QSet<ImageResource*> mPendingImageResources;
    QHash<Tiled::Tileset*, ImageResource*> mImageResources;
};

inline const Tiled::Map *MapResource::map() const
{ return mMap.get(); }

inline const Tiled::TileLayer *MapResource::collisionLayer() const
{ return mCollisionLayer; }

inline const ImageResource *MapResource::tilesetImage(Tiled::Tileset *tileset) const
{ return mImageResources.value(tileset); }

} // namespace Mana

Q_DECLARE_METATYPE(Mana::MapResource*)

#endif // MANA_MAPRESOURCE_H
