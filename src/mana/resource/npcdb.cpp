/*
 * Mana QML plugin
 * Copyright (C) 2012  Erik Schilling 
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include "npcdb.h"

#include "mana/resourcemanager.h"
#include "mana/xmlreader.h"

#include "hairdb.h"
#include "racedb.h"

#include <QDebug>
#include <QNetworkReply>

using namespace Mana;

NpcDB *NpcDB::mInstance = nullptr;

NpcDB::NpcDB(QObject *parent)
    : QObject(parent)
    , mLoaded(false)
{
    Q_ASSERT(!mInstance);
    mInstance = this;
}

void NpcDB::load() const
{
    if (mLoaded)
        return;

    QNetworkReply *reply = ResourceManager::instance()->requestFile(NPC_DB_FILE);

    connect(reply, &QNetworkReply::finished, this, &NpcDB::fileReady);
}

void NpcDB::unload()
{
    mNpcs.clear();

    mLoaded = false;
    emit npcsChanged();
}

void NpcDB::fileReady()
{
    auto *reply = static_cast<QNetworkReply*>(sender());
    reply->deleteLater();
    XmlReader xml(reply);

    if (!xml.readNextStartElement() || xml.name() != QLatin1String("npcs")) {
        qWarning() << "Error loading npcs.xml";
        return;
    }

    while (xml.readNextStartElement()) {
        if (xml.name() == QLatin1String("npc")) {
            const QXmlStreamAttributes atts = xml.attributes();
            int id = atts.value("id").toInt();

            if (!id) {
                qWarning() << "Bad NPC id at line " << xml.lineNumber();
                xml.skipCurrentElement();
                continue;
            }

            QList<SpriteReference *> sprites;
            while (xml.readNextStartElement()) {
                if (xml.name() == QLatin1String("sprite"))
                    sprites.append(SpriteReference::readSprite(xml));
                else
                    xml.readUnknownElement();
            }
            mNpcs[id] = sprites;
        } else {
            xml.readUnknownElement();
        }
    }

    mLoaded = true;
    emit npcsChanged();
    emit loaded();
}
